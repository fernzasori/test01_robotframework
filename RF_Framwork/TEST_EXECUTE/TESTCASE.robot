*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS1_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    https://demostore.x-cart.com/bestsellers/    chrome
    [W] Click Element    //*[@class="header_bar-sign_in"]
    Sleep    3s
    [W] Input Text    //input[@name="login"]    @{ListData}[0]
    [W] Input Text    //input[@name="password"]    @{ListData}[1]
    [W] Click Element    //table[contains(@class,'login-form')]//button[contains(@class,'submit')]
    Sleep    3s
    [W] Click Element    //span[contains(text(),'Fashion')]
    Sleep    5s
    [W] Double Click Element Position    //span[@class='subcategory-name'][contains(text(),'Shoes')]    1
    Sleep    3s
    [W] Click Element    //a[@class='fn url'][contains(.,'White Lace-Up Mesh Trainers')]
    [W] Click Element    //span[contains(.,'Add to cart')]
    Sleep    3s
    [W] Click Element    (//span[contains(.,'View cart')])[2]
    Sleep    3s
    [W] Click Element    //span[contains(.,'Go to checkout')]
    Sleep    3s
    [W] Click Element    (//textarea[@class='form-control secondary'][contains(@id,'note')])[1]
    [W] Input Text    (//textarea[@class='form-control secondary'][contains(@id,'note')])[1]    Test1234
    [W] Click Element    //button[contains(@class,'btn regular-button regular-main-button checkout_fastlane_section-next')]
    Sleep    3s
    [W] Click Element    //button[@class='btn regular-button regular-main-button checkout_fastlane_section-place_order place-order']
    Sleep    3s
    [W] Check Meaasge    Thank you for your order
    Sleep    3s
    [W] Click Element    //a[contains(text(),'My account')]
    [W] Click Element    //ul[@class='account-links dropdown-menu']//span[contains(text(),'Log out')]
    [W] Close Browser
